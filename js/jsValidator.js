/*!
 * jQuery form validation
 * Original author: @gigac
 * Licensed under the MIT license
 */

;(function($, window, document, undefined) {

	/**
	 * @summary Debug function. Outputs in console
	 *
	 * @param mix $var
	 */
	var _d = function( str ) {
		console.log( str );
	};

	// Plugin name constant
	var pluginName = 'gValid';

	$.fn[pluginName] = function(options, r) {
		if( r === true ) {
			var a = new Plugin(this, options, true);
			return a.validateThem();
		}

		// return this.each(function () {
		this.each(function () {
			if(!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, r));
			}
		});
	}

	// Default options
	$.fn[pluginName].options = {
		invalidClass: 'gValid-error',
		validClass: '',
		showErrors: false,
		groupErrors: true,
		holder: '',
		ignore: [],
		events: {},

		errorMessages: null, // function
	};

	$.fn[pluginName].defaultValidation = {
		mail: function(v) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return {
				message: 'E-mail field is required.',
				valid: re.test(v) || !$.trim(v).length
			};
		},

		number: function(v) {
			var re = /^[0-9]+([,.][0-9]+)?$/g;

			return {
				message: 'Enter a valid number.',
				valid: re.test(v) || !$.trim(v).length
			};
		},

		maxlen: function(v, n) {
			return {
				message: "Please enter number less than " + parseInt(n) + ".",
				valid: v.length > parseInt(n) ? false : true
			};
		},

		nonull: function(v) {
			return {
				message: 'This field is required.',
				valid: !$.trim(v).length ? false : true
			};
		}
	};

	$.fn[pluginName].validation = {};

	// Plugin constructor
	function Plugin(element, options, r) {
		this.Form = $( element );

		// Options
		this.options = $.extend({}, $.fn[pluginName].options, options);

		// Validation array of functions
		this.validationFunctions = $.extend($.fn[pluginName].defaultValidation, $.fn[pluginName].validation);

		// Reset errors group
		this.errorsGroup = [];

		// Initialize
		this.init();

		// Bind the events if the plugin is used as a function
		if(r !== true) {
			this.bindEvents();
		}
	}

	// Avoid Plugin.prototype conflicts
	$.extend(Plugin.prototype, {

		/**
		 * Init
		 */
		init: function () {
			this.findFields();
		},

		/**
		 * Find the fields that needs to be validated.
		 */
		findFields: function() {
			if(typeof this.options.ignore == 'string') {
				this.options.ignore = [this.options.ignore];
			}

			if(!Array.isArray(this.options.ignore)) {
				this.options.ignore = [];
			}

			this._fields = this.Form.find('.gValid'); // jQuery array of fields
			this.fields = this._fields.not(this.options.ignore.join(',')); // jQuery array of fields
		},

		/**
		 * Find control element of an input. Control element
		 * is the element where the validation classes
		 * are applied.
		 *
		 * @param {HTMLElement} Input element
		 * @return {jQuery element}
		 */
		findControlElement: function(field) {
			// Convert to jQuery element
			field = $( field );

			// First look for closest control element
			if(field.closest('.'+this.options.holder).length) {
				return field.closest('.'+this.options.holder);
			}

			// If not such an element is found, look inside parent
			if(field.parent().find('.'+this.options.holder).length) {
				return field.parent().find('.'+this.options.holder);
			}

			// return the input as a control element
			return field;
		},

		/**
		 * Loop through fields and validate them
		 */
		validateThem: function() {
			var _this = this;
			var validationFlag = false;

			this.errorsGroup = [];
			$.each(this.fields,function(i, el) {
				if(!_this.validateField(el)) {
					validationFlag = true;
				}
			});

			return !validationFlag;
		},

		/**
		 * Validate single field
		 *
		 * @param {HTMLElement} The single field
		 * @return {boolean}
		 */
		validateField: function(el) {
			var _this = this;
			var field = $(el);
			var controlField = _this.findControlElement(el);
			var validationFlag = true;

			// Do a simple validation if there are no filters provided
			if(typeof field.data('gvalid') === 'undefined' ) {
				field.data('gvalid', 'nonull');
			}

			// Otherwise filter validation. Allow null
			var filters = field.data('gvalid').split('|');
			var msg;
			$.each(filters, function(i, filter) {
				var fv = _this.filterValidation(filter,$.trim(el.value));
				if(!fv.valid) {
					validationFlag = false;
					msg = fv.message;
					controlField.addClass(_this.options.invalidClass);
					return false;
				}
			});

			this.handleError(field, msg, validationFlag);

			if( validationFlag ) {
				controlField.find( '.gValid-error-message' ).remove();
				controlField.removeClass(_this.options.invalidClass);
				return true;
			}

			return false;
		},

		extractFilterName: function(f) {
			return f.indexOf(':') > -1 ? f.substr(0, f.indexOf(':')) : f;
		},

		extractFilterParameters: function(f) {
			return f.indexOf(':') > -1 ? f.substr(f.indexOf(':') + 1) : null;
		},

		filterValidation: function(f, v) {
			var _f = this.extractFilterName(f);
			var _fp = this.extractFilterParameters(f);

			if(typeof this.validationFunctions[_f] === 'function') {
				return this.validationFunctions[_f](v, _fp);
			}

			return {valid: true};
		},

		handleError: function(f, m, v) {
			if(!v) {
				this.errorsGroup.push({field: f, message: m});				
			}

			// If group errors is disabled
			if(!this.options.groupErrors && this.options.showErrors && !v) {
				this.showErrors(f, m);
				this.options.handleError.call(this, f, m);
			}

			// If we are on the last element and group errors is enabled.
			if(this.options.groupErrors && this.fields.index(f) == this.fields.length - 1 && this.options.showErrors) {
				this.showErrors();
			}

			if(this.options.handleError && !v) {
				this.options.handleError.call(this, f, m);
			}
		},

		showErrors: function(f, m) {
			// If group errors is enabled prepend the errors list
			if(this.options.groupErrors) {
				this.Form.find('.gValid-errorList').remove();
				this.Form.prepend(this.errorGroupHTML());
				return;
			}

			// If group errors is disabled append errors individually
			if(!this.options.groupErrors && typeof f != 'undefined' && typeof m != 'undefined') {
				var controlField = this.findControlElement(f);
				controlField.find('.gValid-error-message').remove();
				controlField.prepend(this.errorIndividualHTML(m));
			}
		},

		errorGroupHTML: function() {
			var messages = this.errorsGroup.map(function(a) {
				return a.message;
			});

			var uniqueMessages = messages.filter( function( item, pos, self ) {
				return self.indexOf(item) == pos;
			});

			var errorWrapper = $('<div/>', {
				class: 'gValid-errorList'
			}).append( $('<ul/>').append(
				$.map(uniqueMessages, function(m) {
					return $( '<li/>', { text: m } );
				})
			));

			return errorWrapper;
		},

		errorIndividualHTML: function(m) {
			return $('<span/>', {class: 'gValid-error-message', text: m});
		},

		bindEvents: function() {
			var _this = this;

			$.each(_this.options.events, function(i, event) {
				_this.Form.on(i, function(e) {
					var validation = _this.validateThem();
					if( ! validation ) {
						e.stopImmediatePropagation();
						return false;
					}

					return _this.options.events[i](e);
				});
			});
		}
	});
})(jQuery, window, document);